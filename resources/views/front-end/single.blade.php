
@extends('front-end.layouts.app')

@section('content')
    <!-- innerpages_banner -->
    <div class="innerpages_banner" style="background: linear-gradient(rgba(23, 22, 23, 0), rgba(23, 22, 23, 0)), url({{asset('web/images/banner1.png')}}) repeat;">
        <h2 style="font-size: 55px;
    font-weight: 600;
    color: #fff;
    text-align: center;
    padding-top: 2em;
    padding-bottom: 2em;
    text-shadow: 0 1px 2px rgba(0, 0, 0, .6);">Chi tiết sản phẩm</h2>
    </div>
    <!-- //innerpages_banner -->


    <!-- single -->
    <div class="services">
        <div class="container">
            <div class="col-md-12 single-left">
                <div class="single-left1">
                    <img src="{{asset('storage/product/'.$product->image)}}" alt=" " class="img-responsive" />
                    <h3 style="margin-top: 10px;margin-bottom: 20px;text-align: center;">{{$product->name}}</h3>
                </div>
                <div class="admin">
                    <p style="margin-bottom: 20px;">{!! $product->desc !!}</p>
                    <p style="text-align: center;"><a href="{{route('cart.product', $product->id)}}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>Thêm vào giỏ hàng</a></p>
                </div>

            </div>
        </div>
    </div>
    <!-- //single -->

    <!-- subscribe -->
    <div class="subscribe" style="background-color: #ea2035;">
        <div class="container">
            <h3 class="heading">Subscribe To Get Notifications</h3>
            <div class="subscribe-grid">
                <form action="#" method="post">
                    <input type="email" placeholder="Enter Your Email" name="email" required="">
                    <button class="btn1"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                </form>
            </div>
        </div>
    </div>
    <!-- //subscribe -->

@endsection